import 'dart:io';
import 'package:test_01/test_01.dart' as test_01;

bool isPrime(N) {
  for (var i = 2; i <= N / i; ++i) {
    if (N % i == 0) {
      return false;
    }
  }
  return true;
}

void main() {
  
  bool? result = null;
  print('Please input your number :');
  var N = int.parse(stdin.readLineSync()!);
  if (isPrime(N)) {
    result = true;
  } else {
    result = false;
  }
  print('$result');
}
